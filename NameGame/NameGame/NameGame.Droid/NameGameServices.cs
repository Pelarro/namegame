using System;
using System.Linq;
using System.Collections.Generic;
using NameGame.Droid;
using System.Net;
using Newtonsoft.Json;
using System.IO;
using Xamarin.Forms;
using NameGame.Person;

[assembly: Dependency(typeof(NameGameServices))]
namespace NameGame.Droid
{
    public class NameGameServices : NameGame.INameGameServices
    {
        static private readonly string willowTreePeopleURL = "http://api.namegame.willowtreemobile.com";
        
        static private HttpWebRequest _webRequest;

        public bool PeopleLoaded { get; set; }
        private List<PersonData> _people = new List<PersonData>();
        public void LoadJSON()
        {
            _webRequest = (HttpWebRequest)WebRequest.Create(willowTreePeopleURL);
            _people.Clear();
            PeopleLoaded = false;
            IAsyncResult asyncResult = (IAsyncResult)_webRequest.BeginGetResponse(new AsyncCallback((ar) =>
            {
                HttpWebResponse response = (HttpWebResponse)_webRequest.EndGetResponse(ar);
                string json = new StreamReader(response.GetResponseStream()).ReadToEnd();
                //System.Diagnostics.Debug.WriteLine(json);
                _people = JsonConvert.DeserializeObject<List<PersonData>>(json);
                PeopleLoaded = true;
            }), _webRequest);
        }
        public List<PersonData> GetRandomPeople(int amount)
        {
            return Factory.GetRandomOfEach<PersonData>(_people, amount);
        }
        public List<PersonData> GetRandomPeople(int amount, List<string> excludeNames)
        {
            return Factory.GetRandomOfEach<PersonData>(
                _people.Where(x=> excludeNames.Any(exclude => x.Name.Substring(0, exclude.Length).ToUpper() != exclude.ToUpper())).ToList(), amount);
        }
        public List<PersonData> GetFirstNames(List<string> names)
        {
            return _people.Where(x => names.Any(y => x.Name.Substring(0, y.Length).ToUpper() == y.ToUpper())).ToList();
        }
    }
}