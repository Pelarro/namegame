﻿using NameGame.Person;
using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.IO;

namespace NameGame
{
    static public class Factory
    {
        static public Grid ImageLayout(double imageSize, int perRow, List<PersonImage> personImages)
        {
            Grid imageGrid = new Grid
            {
                BackgroundColor = Color.White,
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
                Padding = 5
            };
            for (int i = 0; i < perRow; i++)
            {
                imageGrid.ColumnDefinitions.Add(new ColumnDefinition
                {
                    Width = new GridLength(imageSize)
                });
            }
            imageGrid.RowDefinitions.Add(new RowDefinition
            {
                Height = new GridLength(imageSize)
            });
            for (int column = 0, row = 0, i = 0; i < personImages.Count; i++, column++)
            {
                if (column >= perRow)
                {
                    imageGrid.RowDefinitions.Add(new RowDefinition
                    {
                        Height = new GridLength(imageSize)
                    });
                    column = 0;
                    row++;
                }

                imageGrid.Children.Add(personImages[i], column, row);
                imageGrid.Children.Add(personImages[i].Overlay, column, row);
                imageGrid.Children.Add(personImages[i].Name, column, row);
            }
            return imageGrid;
        }
        static public PersonImage StartPersonImage()
        {
            Overlay overlay = new Overlay
            {
                Aspect = Aspect.AspectFill,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand,
                IsVisible = false
            };
            Label name = new Label
            {
                FontSize = 23,
                HorizontalTextAlignment = TextAlignment.Center,
                VerticalTextAlignment = TextAlignment.End,
                IsVisible = false,
                TextColor = Color.White
            };
            return new PersonImage
            {
                Scale = 0,
                Aspect = Aspect.AspectFit,
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
                Overlay = overlay,
                Name = name
            };
        }
        static private Random _random = new Random();
        static public List<t> GetRandomOfEach<t>(List<t> startList, int amount)
        {
            if (amount > startList.Count)
                amount = startList.Count;
            List<t> returnList = new List<t>();
            while (returnList.Count < amount)
            {
                int index = _random.Next(0, startList.Count);
                returnList.Add(startList[index]);
                startList.RemoveAt(index);
            }
            return returnList;
        }
    }
}
