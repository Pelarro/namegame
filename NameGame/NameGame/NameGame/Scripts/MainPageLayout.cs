﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace NameGame
{
    public class MainPageLayout
    {
        public Label _whoIsItLabel { get; set; }
        public Label _correctGuesses { get; set; }
        public Label _wrongGuesses { get; set; }
       public Button _cycleButton { get; set; }
        public Label _optionslabel { get; set; }
        public StackLayout CreateTextStackLayout(string labelString, EventHandler call)
        {
            StackLayout textStackLayout = new StackLayout { BackgroundColor = Color.White, Orientation = StackOrientation.Vertical, Spacing = 0 };
            _whoIsItLabel = new Label { HorizontalTextAlignment = TextAlignment.Center, FontSize = 35 };
            textStackLayout.Children.Add(_whoIsItLabel);
            StackLayout underStack = new StackLayout
            {
                BackgroundColor = Color.White,
                Orientation = StackOrientation.Horizontal,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                Spacing = 120
            };
            textStackLayout.Children.Add(underStack);
            StackLayout left = new StackLayout
            {
                BackgroundColor = Color.White,
                Orientation = StackOrientation.Horizontal,
                HorizontalOptions = LayoutOptions.EndAndExpand,
                VerticalOptions = LayoutOptions.Start,
                Spacing = 0
            };
            StackLayout right = new StackLayout
            {
                BackgroundColor = Color.White,
                Orientation = StackOrientation.Vertical,
                HorizontalOptions = LayoutOptions.Start,
                VerticalOptions = LayoutOptions.Start,
                Spacing = 0
            };
            left.Children.Add(OptionsLayout(labelString, call));

            _correctGuesses = new Label { HorizontalTextAlignment = TextAlignment.Start, FontSize = 21, VerticalTextAlignment = TextAlignment.Center,
            TranslationY = 5};
            right.Children.Add(_correctGuesses);
            _wrongGuesses = new Label { HorizontalTextAlignment = TextAlignment.Start, FontSize = 21, VerticalTextAlignment = TextAlignment.Center,
                TranslationY = 5
            };
            right.Children.Add(_wrongGuesses);
            underStack.Children.Add(left);
            underStack.Children.Add(right);
            return textStackLayout;
        }
        public StackLayout OptionsLayout(string labelString,EventHandler call)
        {
            StackLayout optionLayout = new StackLayout
            {
                BackgroundColor = Color.Gray,
                Orientation = StackOrientation.Horizontal,
                HorizontalOptions = LayoutOptions.Start,
                VerticalOptions = LayoutOptions.End,
                WidthRequest = 210,
                HeightRequest = 50,
                Padding = 0
            };
            _cycleButton = new Button
            {
                VerticalOptions = LayoutOptions.Start,
                HorizontalOptions = LayoutOptions.Start,
                FontSize = 18,
                Text = "Next Game",
                TextColor = Color.White,
                BackgroundColor = Color.Black,
                
            };
            //Button down = new Button
            //{
            //    VerticalOptions = LayoutOptions.End,
            //    HorizontalOptions = LayoutOptions.CenterAndExpand,
            //    FontSize = 20,
            //    Text = "v",
            //    TextColor = Color.White,
            //    BackgroundColor = Color.Black
            //};
            _cycleButton.Clicked += call;
            _optionslabel = new Label { VerticalOptions = LayoutOptions.Center, HorizontalOptions = LayoutOptions.Center,
                FontSize = 18, Text = labelString, HorizontalTextAlignment = TextAlignment.Center, TranslationX = -5
            };
            optionLayout.Children.Add(_cycleButton);
            optionLayout.Children.Add(_optionslabel);
            //optionLayout.Children.Add(down);
            return optionLayout;
        }
    }
}
