﻿using System;
using Xamarin.Forms;

namespace NameGame.Person
{
    public class Overlay:Image
    {
        public Overlay() : base()
        {
            Opacity = 0;
        }
        private async void EaseIn()
        {
            IsVisible = true;
            await this.FadeTo(1, 450, Easing.CubicOut);
        }
        public void Correct()
        {
            Source = "correct.png";
            EaseIn();
        }
        public void Wrong()
        {
            Source = "wrong.png";
            EaseIn();
        }
    }
}
