﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms.Internals;

namespace NameGame.Person
{
    [Preserve(AllMembers = true)]
    public class PersonData
    {
        public string URL { get; set; }
        public string Name { get; set; }
    }
}
