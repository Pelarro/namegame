﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using Xamarin.Forms;
using System.Windows.Input;
using System.ComponentModel;
using System.Threading.Tasks;

namespace NameGame.Person
{
    public class PersonImage : Image
    {
        public int DataIndex { get; set; }
        public Overlay Overlay { get; set; }
        public Label Name{get; set;}
        public string URL { get; set; }

        public event EventHandler<PersonImage> IsTappedEvent;
       
        public PersonImage() : base() {
            TapGestureRecognizer tapGesture = new TapGestureRecognizer();
            tapGesture.Tapped += IsTapped;
            GestureRecognizers.Add(tapGesture);
        }
        public void SetAnswer(bool correct)
        {
            Name.IsVisible = true;
            if (correct)
                Overlay.Correct();
            else
                Overlay.Wrong();
        }
        private void IsTapped(object sender, EventArgs e)
        {
            if (IsTappedEvent != null)
                IsTappedEvent.Invoke(sender, this);
        }
        public async void ScaleToSize(uint milliseconds, EventHandler<PersonImage> tap)
        {
            await this.ScaleTo(1, milliseconds, Easing.BounceOut);
            Name.Scale = 1;
            Overlay.Scale = 1;
            IsTappedEvent += tap;
        }
        public async void ScaleDown(uint milliseconds)
        {
            Name.ScaleTo(0, milliseconds, Easing.Linear);
            Overlay.ScaleTo(0, milliseconds, Easing.Linear);
            await this.ScaleTo(0, milliseconds, Easing.Linear);
            Overlay.IsVisible = false;
            Overlay.Opacity = 0;
            Name.IsVisible = false;
        }
        public async Task<ImageSource> LoadImageFromURL(Uri uri)
        {
            URL = uri.OriginalString;
            Task<ImageSource> result = Task<ImageSource>.Factory.StartNew(() => ImageSource.FromUri(uri));
            Source = await result;
            return Source;
        }
    }
}
