﻿using NameGame.Person;
using System.Collections.Generic;

namespace NameGame
{
    public interface INameGameServices
    {
        bool PeopleLoaded { get; set; }
        void LoadJSON();
        List<PersonData> GetRandomPeople(int amount);
        List<PersonData> GetRandomPeople(int amount, List<string> excludeNames);
        List<PersonData> GetFirstNames(List<string> names);
    }
}
