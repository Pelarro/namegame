﻿using NameGame.Person;
using System;
using System.Collections.Generic;

namespace NameGame.Game
{
    public class MattBlitz: BuildGroup
    {
        
        public override List<PersonData> NewGroup(int amount)
        {
            List<PersonData> list = App._NameGameServices.GetFirstNames(new List<string> { "matt", "mat" });
            while (list.Count > amount)
            {
                list.RemoveAt(_random.Next(0, list.Count));
            }
            _correctIndex = _random.Next(0, amount);
            return list;
        }
    }
}
