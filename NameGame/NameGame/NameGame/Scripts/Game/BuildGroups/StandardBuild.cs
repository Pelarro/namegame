﻿using System;
using System.Collections.Generic;
using System.Linq;
using NameGame.Person;

namespace NameGame.Game
{
    public class StandardBuild : BuildGroup
    {
        public override List<PersonData> NewGroup(int amount)
        {
            _correctIndex = _random.Next(0, amount);
            return App._NameGameServices.GetRandomPeople(amount);
        }
    }
}
