﻿using NameGame.Person;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NameGame.Game
{
    public class JNames:BuildGroup
    {
        public override List<PersonData> NewGroup(int amount)
        {
            List<PersonData> list = App._NameGameServices.GetFirstNames(new List<string> { "j" });
            while (list.Count > amount)
            {
                list.RemoveAt(_random.Next(0, list.Count));
            }
            _correctIndex = _random.Next(0, amount);
            return list;
        }
    }
}
