﻿using System.Collections.Generic;
namespace NameGame.Game
{
   public interface IBuildGroup
    {
        List<Person.PersonData> NewGroup(int amount);
        int CorrectIndex { get; }
    }
}
