﻿using System;
using System.Collections.Generic;
using NameGame.Person;

namespace NameGame.Game
{
    public class MattBuild : BuildGroup
    {
        public override List<PersonData> NewGroup(int amount)
        {
            List<PersonData> list = App._NameGameServices.GetRandomPeople(amount - 1, new List<string> { "matt", "mat" });
            List<PersonData> matts = App._NameGameServices.GetFirstNames(new List<string> { "matt", "mat" });
            PersonData mat = matts[_random.Next(0, matts.Count)];
            _correctIndex = _random.Next(0, amount);
            list.Insert(_correctIndex, mat);
            return list;
        }
    }
}
