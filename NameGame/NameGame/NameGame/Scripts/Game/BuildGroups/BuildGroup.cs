﻿using NameGame.Person;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NameGame.Game
{
    public abstract class BuildGroup:IBuildGroup
    {
        protected Random _random = new Random();
        protected int _correctIndex = 0;
        public int CorrectIndex
        {
            get
            {
                return _correctIndex;
            }
        }

        public abstract List<PersonData> NewGroup(int amount);
    }
}
