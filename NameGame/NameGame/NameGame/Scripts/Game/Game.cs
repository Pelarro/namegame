﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NameGame.Person;

namespace NameGame.Game
{
    public class Game
    {
        public int CorrectGuesses{ get; set; }
        public int WrongGuesses { get; set; }
        private IBuildGroup BuildGroup { get; set; }

        private int _correctIndex = 0;
        
        private List<PersonData> _currentData = new List<PersonData>();
        
        private enum State { loading, guessing, unloading};
        private State _state = State.loading;

        public enum BuildType { Standard, FindMatt, MattBlitz, jNames}
        public BuildType CurrentBuildType { get; set; }
        public void NextBuildType()
        {
            int i = (int)CurrentBuildType;
            i++;
            if (i >= System.Enum.GetNames(typeof(BuildType)).Length)
                i = 0;
            CurrentBuildType = (BuildType)i;
        }
        public void SetBuildType()
        {
            switch (CurrentBuildType)
            {
                case BuildType.Standard:
                    BuildGroup = new StandardBuild();
                    break;
                case BuildType.FindMatt:
                    BuildGroup = new MattBuild();
                    break;
                case BuildType.MattBlitz:
                    BuildGroup = new MattBlitz();
                    break;
                case BuildType.jNames:
                    BuildGroup = new JNames();
                    break;
            }
        }
        public string GuessText { get { return "Who is " + _currentData[_correctIndex].Name; } }
        public bool CorrectGuess(int index)
        {
            return index == _correctIndex;
        }

        public async Task<bool> StartGame(List<PersonImage> personImages)
        {
            if (_state == State.guessing)
            {
                await SetupPersonImages(personImages);
                return false;
            }

            await LoadGame(personImages);
            await SetupPersonImages(personImages);
            
            return true;
        }
        private async Task<bool> LoadGame(List<PersonImage> personImages)
        {
            _state = State.loading;
            if (!App._NameGameServices.PeopleLoaded)
            {
                System.Diagnostics.Debug.WriteLine("Waiting For JSON Data");
                await Task.Delay(1000);
                return await LoadGame(personImages);
            }
           
            _currentData = BuildGroup.NewGroup(personImages.Count);
            _correctIndex = BuildGroup.CorrectIndex;
            return true;
        }
        private async Task<bool> SetupPersonImages(List<PersonImage> personImages)
        {
            _state = State.guessing;
            for (int i = 0; i < _currentData.Count; i++)
            {
                personImages[i].Name.Text = _currentData[i].Name;
                await personImages[i].LoadImageFromURL(new Uri(_currentData[i].URL));
                personImages[i].DataIndex = i;
                System.Diagnostics.Debug.WriteLine("Image LOaded");
            }
            
            return true;
        }
        public void UnloadImages()
        {
            
            _state = State.unloading;
        }
        public Game(BuildType build)
        {
            CurrentBuildType = build;
            SetBuildType();
        }
    }
}
