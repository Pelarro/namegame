﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace NameGame
{
    public partial class App : Application
    {
        public static INameGameServices _NameGameServices { get; set; }
        public App()
        {
            _NameGameServices = DependencyService.Get<INameGameServices>();
            _NameGameServices.LoadJSON();
            InitializeComponent();
            MainPage = new NameGame.MainPage();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
