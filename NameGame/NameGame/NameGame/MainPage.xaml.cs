﻿using NameGame.Person;
using NameGame.Game;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace NameGame
{
    public partial class MainPage : ContentPage
    {
        public double width { get; set; }
        public double height { get; set; }

        private MainPageLayout _mainPageLayout = new MainPageLayout();
        private List<PersonImage> _personImages = new List<PersonImage>();

        private int _amountOfPeople = 6;
        private Game.Game _game;

        public MainPage()
        {
            _game = new Game.Game(Game.Game.BuildType.Standard);
            InitializeComponent();
        }
        protected override void OnSizeAllocated(double width, double height)
        {
            base.OnSizeAllocated(width, height);
            if (width != this.width || height != this.height)
            {
                this.width = width;
                this.height = height;

                StackLayout mainStackLayout = new StackLayout { BackgroundColor = Color.White };
                mainStackLayout.Children.Add(_mainPageLayout.CreateTextStackLayout(_game.CurrentBuildType.ToString(), CycleClicked));
                _personImages.Clear();
                mainStackLayout.Children.Add(CreateImageGrid());
                this.Content = mainStackLayout;

                LoadNewPeople();
            }
        }
        
        private async void LoadNewPeople()
        {
            bool newImages = await _game.StartGame(new List<PersonImage>(_personImages));
            _mainPageLayout._wrongGuesses.Text = string.Format("{0} :Wrong Guesses", _game.WrongGuesses);
            _mainPageLayout._correctGuesses.Text = string.Format("{0} :Correct Guesses", _game.CorrectGuesses);
            _mainPageLayout._whoIsItLabel.Text = _game.GuessText;
            
            foreach (PersonImage person in _personImages)
                person.ScaleToSize(newImages ? (uint)600 : 0, PersonIsTapped);

        }
        private async void UnloadPeople()
        {
            _game.UnloadImages();
            await Task.Delay(750);
            List<PersonImage> tempList = Factory.GetRandomOfEach<PersonImage>(new List<PersonImage>(_personImages), _personImages.Count);
            foreach (PersonImage person in tempList)
            {
                person.ScaleDown(500);
                await Task.Delay(75);
            }
            _mainPageLayout._whoIsItLabel.Text = "Who is ---";
            await Task.Delay(500);
            LoadNewPeople();
        }
        
        private void PersonIsTapped(Object sender, PersonImage person)
        {
            bool correct = _game.CorrectGuess(person.DataIndex);
            person.SetAnswer(correct);
            if (correct)
            {
                foreach (PersonImage p in _personImages)
                    p.IsTappedEvent -= PersonIsTapped;
                _game.CorrectGuesses++;
                _mainPageLayout._correctGuesses.Text = string.Format("{0} :Correct Guesses", _game.CorrectGuesses);
                UnloadPeople();
                return;
            }
            _game.WrongGuesses++;
            _mainPageLayout._wrongGuesses.Text = string.Format("{0} :Wrong Guesses", _game.WrongGuesses);
            person.IsTappedEvent -= PersonIsTapped;
        }
        private void CycleClicked(Object sender, EventArgs e)
        {
            _game.NextBuildType();
            _game.SetBuildType();
            _mainPageLayout._optionslabel.Text = _game.CurrentBuildType.ToString();
        }
        public Grid CreateImageGrid()
        {
            int imagesPerRow = 2;
            double dimension = width - 100;
            if (width > height)
            {
                imagesPerRow = 3;
                dimension = height;
            }
            for (int i = 0; i < _amountOfPeople; i++)
            {
                _personImages.Add(Factory.StartPersonImage());
            }
            return Factory.ImageLayout(dimension / imagesPerRow, imagesPerRow, _personImages);
        }
    }
}
